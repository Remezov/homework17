package ru.remezov.library;

import java.io.*;

public class App {
    static String fileName = "library.bin";
    public static void main(String[] args) {
        Book book1 = new Book("Горе от ума", 1989, "А.С. Грибоедов");
        Book book2 = new Book("Война и мир", 2006, "Л.Н. Толстой");
        //saveBooks(book1, book2);
        loadBooks();
    }

    public static void saveBooks(Book... books) {
        System.out.println("Добавляем книги:");
        for (Book book : books) {
            System.out.println(book.toString());
        }
        try (
                FileOutputStream fos = new FileOutputStream(fileName);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
        ) {
            oos.writeObject(books);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Book[] loadBooks() {
        Book[] books = null;
        File file = new File(fileName);
        if (!file.exists()) {
            System.out.println("Библиотека пуста");
            return books;
        }
        try (
                FileInputStream fis = new FileInputStream(fileName);
                ObjectInputStream ois = new ObjectInputStream(fis);
        ) {
            books = (Book[]) ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Данная библитека:");
        for (Book book : books) {
            System.out.println(book.toString());
        }
        return books;
    }
}